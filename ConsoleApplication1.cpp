﻿#include <iostream>

class Animal
{
public:
    virtual void Voice()
    {
        std::cout << "Raw";
    }
};

class Dog : public Animal
{
public:
        void Voice() override
    {
        std::cout << "Woof!"<<"\n";
    }
};

class Cat : public Animal
{
public:
        void Voice() override
    {
        std::cout << "Meow!" << "\n";
    }
};

class Fox : public Animal
{
public:
        void Voice() override
    {
        std::cout << "What does the Fox say?" << "\n";
    }
};
int main()
{
    Animal *MAnimal[3];
    
    Dog Sharik; 
    MAnimal[0] = &Sharik;
    Cat Barsik;
    MAnimal[1] = &Barsik;
    Fox Kolya;
    MAnimal[2] = &Kolya;
    
    MAnimal[0]->Voice();
    MAnimal[1]->Voice();
    MAnimal[2]->Voice();

    return 0;

}


